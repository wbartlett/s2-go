package models

import (
	"encoding/json"
	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
	"github.com/gofrs/uuid"
	"time"
)

// Something model struct
type Something struct {
	ID          uuid.UUID    `json:"id" db:"id"`
	CreatedAt   time.Time    `json:"created_at" db:"created_at"`
	UpdatedAt   time.Time    `json:"updated_at" db:"updated_at"`
	Heading     string       `json:"heading" db:"heading"`
	Title       string       `json:"title" db:"title"`
	Description nulls.String `json:"description" db:"description"`
	Website     string       `json:"website" db:"website"`
	ImageURL    string       `json:"image_url" db:"image_url"`
	Confirmed   bool         `json:"confirmed" db:"confirmed"`
	Active      bool         `json:"active" db:"active"`
}

// String is not required by pop and may be deleted
func (s Something) String() string {
	js, _ := json.Marshal(s)
	return string(js)
}

// Somethings is not required by pop and may be deleted
type Somethings []Something

// String is not required by pop and may be deleted
func (s Somethings) String() string {
	js, _ := json.Marshal(s)
	return string(js)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (s *Something) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.StringIsPresent{Field: s.Heading, Name: "Heading"},
		&validators.StringIsPresent{Field: s.Title, Name: "Title"},
		&validators.StringIsPresent{Field: s.Website, Name: "Website"},
		&validators.StringIsPresent{Field: s.ImageURL, Name: "ImageURL"},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (s *Something) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (s *Something) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}
