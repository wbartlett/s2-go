package grifts

import (
	"bitbucket.org/wbartlett/s2/actions"
	"github.com/gobuffalo/buffalo"
)

func init() {
	buffalo.Grifts(actions.App())
}
